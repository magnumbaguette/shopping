class Shop {
    private String itemName;
    private float price;
    private String type; // Probably make this a whole new class and compose it here.
    Discount discountType;
    private String customerName;

   Shop() {
       this("placeholderName", 13.43f, "genderPlaceholder", "placeholderName");
    }

    private Shop(String placeholderName, float v, String genderPlaceholder, String placeHolderCustomerName) {
        this.itemName = placeholderName;
        this.price = v;
        this.type = genderPlaceholder;
        this.customerName = placeHolderCustomerName;
    }

    float calculateDiscount() {
        return discountType.discount();
    }

    void setDiscountType(Discount newDiscountType) {
        this.discountType = newDiscountType;
    }

    void printInfo() {
       System.out.println(itemName + " " + price + " " + type + " " + discountType.discount() + " " + customerName);
    }
}
