class Cart extends Shop {
    float price = 1f;

    Cart(Discount setDiscountType) {
        discountType = setDiscountType;
    }
    // have to init the super class constructor too.
    Cart() {
        discountType = new NoDiscount();
    }

    //logic to add items to the cart and (or) manage items in the cart.

    void checkOut() {
        System.out.println("You total price to pay is: " + calculateDiscount()*price);
    }
}
