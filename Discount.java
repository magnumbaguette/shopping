public interface Discount {
    float discount();
}

class Men implements Discount {
    public float discount() {
        return 5f;
    }
}

class Women implements Discount {
    public float discount() {
        return 5.5f;
    }
}

class NoDiscount implements Discount {
    public float discount() {
        return 0f;
    }
}
